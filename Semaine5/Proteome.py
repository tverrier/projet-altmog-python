# -*- coding: utf-8 -*-
"""
Created on Sun Nov 15 21:59:32 2020

@author: thiba
"""

#Entry	Protein names	Gene names	Organism	Length	Entry name

import csv
import urllib.request
import tempfile
import re
import shutil

class Proteome:
    prot_list = []
    prot_name_dict = {}
    uniprot_ID_dict = {}

    
    def __init__(self,filename):
        self.filename = filename
        self.prot_name_dict = self.get_proteins_name_dict()
        self.prot_list = self.get_proteins_list()
        self.uniprot_ID_dict = self.get_uniprot_ID_dict()
    
    

    

    def get_proteins_name_dict(self):
        """
        Reads the proteome file and returns a dictionnary with protein names as key and their UniprotID as value.

        Returns
        -------
        prot_name_dict : dict
            dict = {protein_name : UniprotID}

        """
        #Initialization of the returned dictionnary
        prot_name_dict = {}
        
        #The file is read, first line is discarded (headers), and read as CSV file
        with open(self.filename, "r") as proteome_file:
            proteome_file.readline()
            proteome_file_reader = csv.reader(proteome_file, delimiter = '\t')
            
        #col[5] contains protein names and col [0] the UniprotID. Check if there is a duplicate and 
        #create a new entry for unseen unseen protein names, with UniprotID as value if not already in values
            for line_list in proteome_file_reader:
                if line_list[5] in prot_name_dict :
                    if line_list[0] not in prot_name_dict[line_list[5]] :
                        prot_name_dict[line_list[5]].append(line_list[0])
                else: prot_name_dict[line_list[5]] = line_list[0]
        proteome_file.close()
        return prot_name_dict
    
    def get_proteins_list(self):
        """
        Returns the list of all the proteome's protein names.

        Returns
        -------
        prot_list : list
            list of all the proteome's protein names.

        """
        prot_list = list(self.prot_name_dict.keys())  
        return prot_list
    
    def get_uniprot_ID_dict(self):
        """
        Reads the proteome file and returns a dictionnary with UniprotID as key and their protein names as value.

        Returns
        -------
        uniprot_ID_dict : dict
            dict = {UniprotID : protein_name}.

        """
        #Initialization of the returned dictionnary
        uniprot_ID_dict = {}
        
        #The file is read, first line is discarded (headers), and read ad CSV file
        with open(self.filename, "r") as proteome_file:
            proteome_file.readline()
            proteome_file_reader = csv.reader(proteome_file, delimiter = '\t')
            
        #col[5] contains protein names and col [0] the UniprotID. Check if there is a dupliacate and 
        #create a new entry for each unseen UniprotID, with protein names as value if not already in values.
            for line_list in proteome_file_reader:
                if line_list[0] in uniprot_ID_dict :
                    if line_list[5] not in uniprot_ID_dict[line_list[0]] :
                        uniprot_ID_dict[line_list[0]].append(line_list[5])
                else: uniprot_ID_dict[line_list[0]] = line_list[5]       
        proteome_file.close()
        return uniprot_ID_dict
      
   
    def get_protein_domains(self, p):
        """
        Returns a list of all pfam domains identified for the UniprotID p.

        Parameters
        ----------
        p : str
            Proteine's UniprotID.

        Returns
        -------
        domain_names_list : list
            list of pfam domains associated with the uniprotID p

        """
        #URL using p
        url_str = "https://www.uniprot.org/uniprot/{}.txt".format(p)
        
        #Read and stocks the HTML text page in the file variable as str
        with urllib.request.urlopen(url_str) as response:
            with tempfile.NamedTemporaryFile(delete=False) as tmp_file:
                shutil.copyfileobj(response, tmp_file)
                with open(tmp_file.name) as html:
                    file = html.read()
        #Use RE to fine the lines beginning by "DR   Pfam" then stocks them into a list
        results = re.findall(r'DR\s{3}Pfam;\s[A-Z\d]+;\s[\dA-Za-z_-]+', file)
        
        #4th elements of each lines (domain name) are extracted and returned as a list
        domain_names_list=[]
        for i_str in results:
                domain_names_list.append(i_str.split()[3])
        return domain_names_list
        
          
test = Proteome("uniprot_human.tab")
uniprot_test = "P31946"
uniprot_test2="P06213"                 
get_protein_domains_test = test.get_protein_domains(uniprot_test2)  

#print("https://www.uniprot.org/uniprot/{}.txt".format(uniprot_test))          
       

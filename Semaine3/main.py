# -*- coding: utf-8 -*-
"""
Created on Thu Oct 15 18:45:08 2020

@author: thiba
"""
import Read_interaction_file_functions as Riff
import unittest

file_test = "Human_HighQuality" # includes 1 homodimere and 1 missing value and 1 duplicate
test_obj = Riff.Interactome(file_test)
deg = 2
prot_str = "1433E_HUMAN"
#test = "Human_HighQuality"
    #"toy_example_missing_value_duplicate" 

test_obj.writeCC()




class test_Read_interaction_file_class_return(unittest.TestCase):
    
    def test_dictionnary(self):
        self.assertEqual(type(test_obj.int_dict), dict, "read_interaction_file_dict failed, dict expected")
        
    def test_list(self):
        self.assertEqual(type(test_obj.int_list), list, "read_interaction_file_list failed, list expected")
        
    def test_count_vertices(self):
        resultOfFunction = test_obj.count_vertices()
        self.assertEqual(type(resultOfFunction), int, "count_vertices failed, int expected")
        
    def test_count_edges(self):
        resultOfFunction = test_obj.count_edges()
        self.assertEqual(type(resultOfFunction), int, "count_edges failed, int expected")
        
    def test_get_degree(self):
        resultOfFunction = test_obj.get_degree(prot_str)
        self.assertEqual(type(resultOfFunction), int, "get_degree failed, int expected")
        
    def test_get_max_degree(self):
        resultOfFunction = test_obj.get_max_degree()
        self.assertEqual(type(resultOfFunction), tuple, "get_max_degree failed, tuple expected")
    
    def test_get_avg_degree(self):
        resultOfFunction = test_obj.get_ave_degree()
        self.assertEqual(type(resultOfFunction), float, "get_ave_degree failed, int expected")
        
    def test_count_deg(self):
        resultOfFunction = test_obj.count_deg( deg)
        self.assertEqual(type(resultOfFunction), int, "count_deg failed, int expected")

    '''
    issue : Interactome object cannot be created if the file.txt has missing values (indexation error generated for read functions), 
    hence using clean_interactome() on interactome object alone is redundant, the object has already been cleaned during the object instenciation if needed. 
    '''
    def test_clean_interactome_interaction_number(self):
        test_obj.clean_interactome()
        with open("{}_cleaned.txt".format(file_test), "r") as fileToClean :
            lines = fileToClean.readlines()
        self.assertTrue(int(lines[0]) == (len(lines)-1) , " clean_interactome failed,  ")
 
    
    def test_clean_interactome_file(self):
        self.assertTrue(test_obj.is_interaction_file(), "clean_interactome file generating failed")
        
    def test_countCC_computeCC(self):
        self.assertTrue(str(len(set(test_obj.computeCC()))) == str(len(test_obj.countCC())), " Number of CC is not consistent between countCC and computeCC functions" )
        
        
unittest.main()

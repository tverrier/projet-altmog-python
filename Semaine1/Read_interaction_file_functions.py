# -*- coding: utf-8 -*-
"""
Created on Fri Sep 25 20:46:23 2020

@author: thiba
"""
import os

class Interactome:
    def __init__(self, file):
        self.filename = "{}.txt".format(file)     
        self.int_list = self.read_interaction_file_list()
        self.int_dict = self.read_interaction_file_dict()
        self.proteins = list(self.int_dict.keys())
        
        
        
    def read_interaction_file_dict(self):
        '''
        Reads the interaction file og the interactome object and returns a dictionnary 
        of each summit with their direct neighbors as values.
    
        Parameters
        ----------
    
        Returns
        -------
        dict_inter : Dictionnary
            Dictionnary with elements as keys and their direct neighbor as values.
    
        '''
        dict_inter = {}
        with open(self.filename, "r") as file_int:
            
            next(file_int)
            for line in file_int:
                
                summits = line.split()
                s1 = summits[0]
                s2 = summits[1]
                if s1  in dict_inter:
                    if s2 not in dict_inter[s1]:
                        dict_inter[s1].append(s2)
                    else : dict_inter[s1] = [s2]
                else : dict_inter[s1] = [s2]
                if s2  in dict_inter:
                    if s1 not in dict_inter[s2]:
                        dict_inter[s2].append(s1)
                    else : dict_inter[s2] = [s1]
                else : dict_inter[s2] = [s1]
        file_int.close()
        return dict_inter

                    
    def read_interaction_file_list(self):
        '''
        Reads the interaction file of the interactom object and returns a list of all pair of 
        interacting elements.
    
        Parameters
        ----------
    
        Returns
        -------
        list_inter : List
            List of all pair of interacting elements.
    
        '''
        list_inter = []
        with open(self.filename, "r") as file_int:
            next(file_int)
            for line in file_int:
                summits = line.split()
                list_inter.append((summits[0],summits[1]))
        return list_inter
                 
    

    def is_interaction_file(self):
        """
        Test if the interactome object has the interaction file format, and returns True or False.
    
        Parameters
        ----------
    
        Returns
        -------
        bool
            True if the file has the interaction file format. Returns False if not.
    
        """
        if os.stat(self.filename).st_size != 0 : 
            with open(self.filename, "r") as file_to_test:
                first_line = file_to_test.readline()
                first = first_line.split()
                if len(first) != 1 : 
                    print("first line is not the interaction number")
                    return False
                try :
                    nbInteractions = int(first[0])
                except ValueError :	
                    print("The file has NO NUMBER of interactions")
                    return False
                nbLine = 0            
                for line in file_to_test:
                    nbLine += 1
                    interaction = line.split()
                    if len(interaction) != 2: 
                        print("error line : ", nbLine)
                        return False
                if nbLine != nbInteractions :
                    print("Number of interaction is different from expected")
                    return False
            return True   
        else: 
            print("Empty file as argument")    
            return False

    def count_vertices(self):
        """
        Count and return the number of vertices from the interactome object.
    
        Parameters
        ----------
        file : str
            Interaction text file name.
    
        Returns
        -------
        nb_vertice_int : int
            Number of vertices in the interaction file.
    
        """
        nb_vertice_int = len(self.proteins)
        return nb_vertice_int
    
    def count_edges(self):
        """
        Count and return the number of edges from the interactome object.
    
        Parameters
        ----------
        file : str
            Interaction text file name.
    
        Returns
        -------
        nb_edges_int : int
            Number of edges in the interaction file.
    
        """
        #Do not forget to clean the file beforehand to avoid duplicated interactions
        nb_edges_int = len(self.int_list)
        return nb_edges_int
    
    
    def clean_interactome(self):
        """
        Clean the specified interaction file by writing a new interaction file without 
        redundant or homodimeric interactions.
    
        Parameters
        ----------
        file : str
            Interaction text file's name to clean.
    
        Returns
        -------
        None.
    
        """
        interactions_seen_list = []
        nb_interactions_int = 0
        with open(self.filename, "r") as file_to_clean :
            file_to_clean.readline()
            for line in file_to_clean:
                inter_list = line.split()
                #check if the interaction has been already seen
                if tuple(inter_list) and tuple(inter_list[::-1]) not in interactions_seen_list:
                    #check that the line has 2 elements, and then if the interaction is not an homodimere 
                    if len(inter_list) == 2 :
                        if inter_list[0] != inter_list[1] :
                            nb_interactions_int +=1
                            interactions_seen_list.append(tuple(inter_list))
            newfile = open("{}_cleaned.txt".format(self.filename), "w")
            newfile.write("{0} \n".format(nb_interactions_int))
            for l in interactions_seen_list:
                prot1_str = l[0]
                prot1_str = l[1]
                newfile.write("{0}   {1}\n".format(prot1_str, prot1_str))
        newfile.close()
        return 
    
    def get_degree(self, prot_str):
        """
        Compute the degree of the specified protein from the interactome object.
    
        Parameters
        ----------
        prot_str : str
            Name of the protein of interest.
    
        Returns
        -------
        int
            Degree of the specified protein.
    
        """
        #check if the protein is present as a key
        if prot_str not in self.int_dict:
            print("No such protein in file")
        else : 
            return len(self.int_dict[prot_str])
    
    
    
    def get_max_degree(self):
        """
        Exctract the protein which has the greater degree from the 
        specified interaction file and returns its name.
    
        Parameters
        ---------
        
        Returns
        -------
        prot_str : str
            Name of the protein with maximum number of degree.
        max_degree_int : int
            Degree of returned protein.
    
        """
        max_degree_int = 0
        prot_str = ""
        for p in self.int_dict:
            if len(self.int_dict[p]) > max_degree_int:
                max_degree_int = len(self.int_dict[p])
                prot_str = p
        return (prot_str, max_degree_int)
    
    def get_ave_degree(self):
        """
        Compute the average degree of all proteins in the interaction file and 
        return it.
    
        Parameters
        ----------
    
        Returns
        -------
        ave_degree_float : float
            average degree of all proteins.
    
        """
        nb_prot_int = 0
        for p in self.int_dict:
            nb_prot_int += 1
        ave_degree_float = float(len(self.int_list[1]))/float(nb_prot_int) 
        return ave_degree_float
    # 2x more interaction - expected 
    
    def count_deg(self, deg):
        """
        Compute and return the number of protein which have the same degree as specified. 
    
        Parameters
        ----------
        deg : int
            Number of degree to look for.
    
        Returns
        -------
        int
            Number of protein that have the specified degree.
    
        """
        protein_list = []
        for p in self.int_dict :
            if len(self.int_dict[p]) == deg:
                protein_list.append(p)
        return len(protein_list)
    
    def histogramm_degree(self, dmin, dmax):
        """
        Compute, for each degree d within [dmin, dmax], the number of protein
        of d degree. Print the corresponding histogramm.
    
        Parameters
        ----------
        
        dmin : int
            Minimum degree.
        dmax : int
            Maximum degree.
    
        Returns
        -------
        None.
    
        """
        protein_count_list = []
        for d in range(dmin,dmax+1):
            protein_count_list.append((d,self.count_deg(d)))
        for i in protein_count_list:
            output_str = ""
            time_int = i[1]
            while(time_int > 0):
                output_str += "*"
                time_int = time_int-1
            print(i[0], output_str)
        return    
    #most proteins interact with very few proteins


test = "Human_HighQuality"

first_file = Interactome(test)
print(first_file.is_interaction_file())
dico = first_file.int_dict
print(first_file.proteins)
print(first_file.get_degree("CLD16_HUMAN"))
print(first_file.get_ave_degree())
name = first_file.filename

first_file.histogramm_degree(1,50)
print(first_file.is_interaction_file())
## 

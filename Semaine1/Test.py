# -*- coding: utf-8 -*-
"""
Created on Tue Sep 29 17:55:45 2020

@author: thiba
"""

# Test file for read_interaction_file_functions


import Read_interaction_file_functions
import unittest
file = "Human_HighQuality.txt"

class test_Read_interaction_file_class_return(unittest.TestCase):
    
    def test_dictionnary(self):
        resultOfFunction = Read_interaction_file_functions.read_interaction_file_dict(file)
        self.assertEqual(type(resultOfFunction), dict, "read_interaction_file_dict failed, dict expected")
        
    def test_list(self):
        resultOfFunction = Read_interaction_file_functions.read_interaction_file_list(file)
        self.assertEqual(type(resultOfFunction), list, "read_interaction_file_list failed, list expected")
        
    def test_tuple(self):
        resultOfFunction = Read_interaction_file_functions.read_interaction_file(file)
        self.assertEqual(type(resultOfFunction), tuple, "read_interaction_file failed, tuple expected")
        
        
unittest.main()
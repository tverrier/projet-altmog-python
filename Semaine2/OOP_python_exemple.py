# -*- coding: utf-8 -*-
"""
Created on Fri Oct  2 13:24:25 2020

@author: thiba
"""

## OOP with python

class Adress:
    
    def __init__(self, num, street, zipcode, city):
        self.street_nb = num
        self.street_name = street
        self.zip_nb = zipcode
        self.city_name = city
       
    def print_adress(self):
        #self = use class attribute (like static in java)
        print("l'adresse est " + str(self.street_nb) + " " + self.street_name)
        
    def __str__(self):
        return("adresse : " + str(self.street_nb) + " " + self.street_name)
        
instanceAdress = Adress(12, "Bd baille", 13006, "Marseille")
instanceAdress.print_adress()
print(instanceAdress)
#on a surcharger print()
"""
methode private existe aussi en débutant le nom avec "_"
methode __XX__ sont héritées et on les surcharge
Attribut de class stocké dans un dictionnaire, dir montre ses clefs
on peut cree des attributs XXX à la volé (même si utilisation tres particuliere):
    dans la methode : 
        self.XXX = "kekchose"
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Oct  1 17:34:56 2020

@author: thiba
"""

import Graph_interaction as Gi
import reading_graph_interactions_proteins as Riff

import unittest
file = "Human_HighQuality.txt"
prot_str = "1433B_HUMAN"
deg = 5

class test_Graph_interaction(unittest.TestCase):
    
    def test_count_vertices(self):
        resultOfFunction = Gi.count_vertices(file)
        self.assertEqual(type(resultOfFunction), int, "count_vertices failed, int expected")
        
    def test_count_edges(self):
        resultOfFunction = Gi.count_edges(file)
        self.assertEqual(type(resultOfFunction), int, "count_edges failed, int expected")
        
    def test_get_degree(self):
        resultOfFunction = Gi.get_degree(file, prot_str)
        self.assertEqual(type(resultOfFunction), int, "get_degree failed, int expected")
        
    def test_get_max_degree(self):
        resultOfFunction = Gi.get_max_degree(file)
        self.assertEqual(type(resultOfFunction), tuple, "get_max_degree failed, tuple expected")
    
    def test_get_avg_degree(self):
        resultOfFunction = Gi.get_ave_degree(file)
        self.assertEqual(type(resultOfFunction), float, "get_ave_degree failed, int expected")
        
    def test_count_deg(self):
        resultOfFunction = Gi.count_deg(file, deg)
        self.assertEqual(type(resultOfFunction), int, "count_deg failed, int expected")

    def test_clean_interactome_interaction_number(self):
        file = "newfile.txt"
        Gi.clean_interactome("toy_example_missing_value_duplicate.txt")
        with open(file, "r") as fileToClean :
            lines = fileToClean.readlines()
        self.assertTrue(int(lines[0]) == (len(lines)-1) , " clean_interactome failed,  ")
        
    def test_clean_interactome_file(self):
        Gi.clean_interactome(file)
        self.assertTrue(Riff.is_interaction_file("newfile.txt"), "clean_interactome file generating failed")
        

    
   

unittest.main()
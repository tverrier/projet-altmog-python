# -*- coding: utf-8 -*-
"""
Created on Tue Sep 29 21:02:52 2020

@author: thiba
"""


import Read_interaction_file_functions as Riff

#33333
def count_vertices(file):
    """
    Count and return the number of vertices from the specified interaction text file.

    Parameters
    ----------
    file : str
        Interaction text file name.

    Returns
    -------
    nb_vertice_int : int
        Number of vertices in the interaction file.

    """
    inter_dict = Riff.read_interaction_file_dict(file)
    nb_vertice_int = len(inter_dict)
    return nb_vertice_int

def count_edges(file):
    """
    Count and return the number of edges from the specified interaction text file.

    Parameters
    ----------
    file : str
        Interaction text file name.

    Returns
    -------
    nb_edges_int : int
        Number of edges in the interaction file.

    """
    #Do not forget to clean the file beforehand to avoid duplicated interactions
    inter_list = Riff.read_interaction_file_list(file)
    nb_edges_int = len(inter_list)
    return nb_edges_int


def clean_interactome(file):
    """
    Clean the specified interaction file by writing a new interaction file without 
    redundant or homodimeric interactions.

    Parameters
    ----------
    file : str
        Interaction text file's name to clean.

    Returns
    -------
    None.

    """
    interactions_seen_list = []
    nb_interactions_int = 0
    with open(file, "r") as file_to_clean :
        file_to_clean.readline()
        for line in file_to_clean:
            inter_list = line.split()
            #check if the interaction has been already seen
            if tuple(inter_list) and tuple(inter_list[::-1]) not in interactions_seen_list:
                #check that the line has 2 elements, and then if the interaction is not an homodimere 
                if len(inter_list) == 2 :
                    if inter_list[0] != inter_list[1] :
                        nb_interactions_int +=1
                        interactions_seen_list.append(tuple(inter_list))
        newfile = open("newfile.txt", "w")
        newfile.write("{0} \n".format(nb_interactions_int))
        for l in interactions_seen_list:
            prot1_str = l[0]
            prot1_str = l[1]
            newfile.write("{0}   {1}\n".format(prot1_str, prot1_str))
    newfile.close()
    return 

def get_degree(file, prot_str):
    """
    Compute the degree of the specified protein from the specified interaction text file.

    Parameters
    ----------
    file : str
        Interaction text file name.
    prot_str : str
        Name of the protein of interest.

    Returns
    -------
    int
        Degree of the specified protein.

    """
    inter_dict = Riff.read_interaction_file_dict(file)
    #check if the protein is present as a key
    if prot_str not in inter_dict:
        print("No such protein in file")
    else : 
        return len(inter_dict[prot_str])



def get_max_degree(file):
    """
    Exctract the protein which has the greater degree from the 
    specified interaction file and returns its name.

    Parameters
    ----------
    file : str
        Interaction text file name.

    Returns
    -------
    prot_str : str
        Name of the protein with maximum number of degree.
    max_degree_int : int
        Degree of returned protein.

    """
    inter_dict = Riff.read_interaction_file_dict(file)
    max_degree_int = 0
    prot_str = ""
    for p in inter_dict:
        if len(inter_dict[p]) > max_degree_int:
            max_degree_int = len(inter_dict[p])
            prot_str = p
    return (prot_str, max_degree_int)

def get_ave_degree(file):
    """
    Compute the average degree of all proteins in the interaction file and 
    return it.

    Parameters
    ----------
    file : str
        Interaction text file name.

    Returns
    -------
    ave_degree_float : float
        average degree of all proteins.

    """
    inter_global_list = Riff.read_interaction_file(file)
    nb_prot_int = 0
    for p in inter_global_list[0]:
        nb_prot_int += 1
    ave_degree_float = float(len(inter_global_list[1]))/float(nb_prot_int) 
    return ave_degree_float
# 2x more interaction - expected 

def count_deg(file, deg):
    """
    Compute and return the number of protein which have the same degree as specified. 

    Parameters
    ----------
    file : str
        Interaction text file name.
    deg : int
        Number of degree to look for.

    Returns
    -------
    int
        Number of protein that have the specified degree.

    """
    inter_dict = Riff.read_interaction_file_dict(file)
    protein_list = []
    for p in inter_dict :
        if len(inter_dict[p]) == deg:
            protein_list.append(p)
    return len(protein_list)

def histogramm_degree(file, dmin, dmax):
    """
    Compute, for each degree d within [dmin, dmax], the number of protein
    of d degree. Print the corresponding histogramm.

    Parameters
    ----------
    file : str
        Interaction text file name.
    dmin : int
        Minimum degree.
    dmax : int
        Maximum degree.

    Returns
    -------
    None.

    """
    protein_count_list = []
    for d in range(dmin,dmax+1):
        protein_count_list.append((d,count_deg(file, d)))
    for i in protein_count_list:
        output_str = ""
        time_int = i[1]
        while(time_int > 0):
            output_str += "*"
            time_int = time_int-1
        print(i[0], output_str)
    return    
#most proteins interact with very few proteins

    


file = "toy_example.txt"
file2 = "Human_HighQuality.txt"





        

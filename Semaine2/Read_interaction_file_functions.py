# -*- coding: utf-8 -*-
"""
Created on Fri Sep 25 20:46:23 2020

@author: thiba
"""
import os

def read_interaction_file_dict(file):
    '''
    Reads the interaction file in argument and returns a dictionnary 
    with their direct neighbors as values.

    Parameters
    ----------
    file : text file
        Interaction file in text format.

    Returns
    -------
    dictInter : Dictionnary
        Dictionnary with elements as keys and their direct neighbor as values.

    '''
    dictInter = {}
    with open(file, "r") as fileInt:
        
        next(fileInt)
        for line in fileInt:
            
            sommets = line.split()
            s1 = sommets[0]
            s2 = sommets[1]
            if s1  in dictInter:
                if s2 not in dictInter[s1]:
                    dictInter[s1].append(s2)
                else : dictInter[s1] = [s2]
            else : dictInter[s1] = [s2]
            if s2  in dictInter:
                if s1 not in dictInter[s2]:
                    dictInter[s2].append(s1)
                else : dictInter[s2] = [s1]
            else : dictInter[s2] = [s1]
    fileInt.close()
    return dictInter
#cf switch case ?
                    
def read_interaction_file_list(file):
    '''
    Reads the interaction file in argument and returns a list of all pair of 
    interacting elements.

    Parameters
    ----------
    file : text file
        Interaction file in text format.

    Returns
    -------
    listInter : List
        List of all pair of interacting elements.

    '''
    listInter = []
    with open(file, "r") as fileInt:
        next(fileInt)
        for line in fileInt:
            sommets = line.split()
            listInter.append((sommets[0],sommets[1]))
    return listInter
             
def read_interaction_file(file):
    '''
    Reads interaction file in argument and return a tuple containing a 
    dictionnary of all elements and their direct neighbors, and a list of all
    pair of interacting elements

    Parameters
    ----------
    file : interaction text file
        Interaction file in text format.

    Returns
    -------
    d_int : dictionnary
        Dictionnary with elements as keys and their direct neighbor as values.
    l_int : list
        List of all pair of interacting elements.

    '''
    d_int = read_interaction_file_dict(file)
    l_int = read_interaction_file_list(file)
    return (d_int, l_int)      

def is_interaction_file(file):
    """
    Test if the file argument has the interaction file format, and returns True or False.

    Parameters
    ----------
    file : interaction text file 
        File to test.

    Returns
    -------
    bool
        True if the file has the interaction file format. Returns False if not.

    """
    if os.stat(file).st_size != 0 : 
        with open(file, "r") as fileToTest:
            firstLine = fileToTest.readline()
            first = firstLine.split()
            if len(first) != 1 : 
                print("first line is not the interaction number")
                return False
            try :
                nbInteractions = int(first[0])
            except ValueError :	
                print("The file has NO NUMBER of interactions")
                return False
            nbLine = 0            
            for line in fileToTest:
                nbLine += 1
                interaction = line.split()
                if len(interaction) != 2: 
                    print("error line : ", nbLine)
                    return False
            if nbLine != nbInteractions :
                print("Number of interaction is different from expected")
                return False
        return True   
    else: 
        print("Empty file as argument")    
        return False


## 
